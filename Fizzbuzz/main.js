// Retrieve user data from local storage
const username = sessionStorage.getItem('user');
let score = sessionStorage.getItem('score');
const apiurl = "http://basic-web.dev.avc.web.usf.edu/" + username;
const scorecontainer = document.getElementById('scoreContainer');

document.addEventListener('DOMContentLoaded', function() {
   
    // Display user information
    document.getElementById('welcomeMessage').textContent = "Welcome, " + username;
    displayscore(score);


    document.getElementById('incrementScoreBtn').addEventListener('click', function() {
        // Increment score in local storage
        score++;
        postscore(score,apiurl);
    });
});

//function for posting the score to the server and updating the score
function postscore(newscore,apiurl){
    post(apiurl, {score : newscore}).then(function(response){
    switch(response.status){
      case 200:
        //user created successfully
        sessionStorage.setItem('score', newscore);
        displayscore(newscore);
        console.log("user updated")
        break;
      case 500:
        console.log("server error, 500")
        break;
    }
  });
}

//function for formatting the score
function displayscore(score){
    if (score == 0){
        scorecontainer.textContent = `Your current score is: 0`;
    }
    else if(score % 15 == 0){
        scorecontainer.textContent = `Your current score is: FizzBuzz`;
    }
    else if(score % 5 == 0){
        scorecontainer.textContent = `Your current score is:  Buzz`;
    }
    else if(score % 3 == 0){
        scorecontainer.textContent = `Your current score is:  Fizz`
    }
    else{
        scorecontainer.textContent = `Your current score is: ${score}`;
    }
}

function get(url) {
    return new Promise((resolve, reject) => {
      const http = new XMLHttpRequest();
      http.onload = function() {
        resolve({ status: http.status, data: JSON.parse(http.response) });
      };
      http.open("GET", url);
      http.send();
    });
}
  
function post(url, data) {
    data = JSON.stringify(data);
    return new Promise((resolve, reject) => {
      const http = new XMLHttpRequest();
      http.onload = function() {
        resolve({ status: http.status, data: JSON.parse(http.response) });
      };
      http.open("POST", url);
      //Make sure that the server knows we're sending it json data.
      http.setRequestHeader("Content-Type", "application/json");
      http.send(data);
    });
}