document.getElementById('loginForm').addEventListener('submit', function(event) {
    //prevents null username
    event.preventDefault();
    //save username and update URL
    const username = document.getElementById('username').value;
    const apiurl = `http://basic-web.dev.avc.web.usf.edu/${username}`;
    //check if user exists
    get(apiurl).then(response => {
        if (response.status === 200) {
            // User exists
            saveUserData(username,response.data.score);
        } else {
            // User does not exist, create the user
            post(apiurl, {score: 0}).then(postResponse => {
                saveUserData(username,0);
            });
        }
    });
});

//function for saving the fetched data to session storage and relocating to the game page
function saveUserData(user,score) {
    sessionStorage.setItem('user', user);
    sessionStorage.setItem('score', score)
    window.location.href = '../Fizzbuzz/index.html'; // Navigate to another webpage
}


function get(url) {
    return new Promise((resolve, reject) => {
      const http = new XMLHttpRequest();
      http.onload = function() {
        resolve({ status: http.status, data: JSON.parse(http.response) });
      };
      http.open("GET", url);
      http.send();
    });
}
  
function post(url, data) {
    data = JSON.stringify(data);
    return new Promise((resolve, reject) => {
      const http = new XMLHttpRequest();
      http.onload = function() {
        resolve({ status: http.status, data: JSON.parse(http.response) });
      };
      http.open("POST", url);
      //Make sure that the server knows we're sending it json data.
      http.setRequestHeader("Content-Type", "application/json");
      http.send(data);
    });
}
